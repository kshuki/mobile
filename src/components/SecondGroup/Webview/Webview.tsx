import React from 'react';
import {Text, View} from 'react-native';

import styles from './WebviewStyles';

const Webview: React.FC = () => {
  return (
    <View style={styles.container}>
      <Text>Workspace</Text>
    </View>
  );
};

export default Webview;
