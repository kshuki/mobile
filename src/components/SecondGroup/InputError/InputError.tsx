import React from 'react';
import {View} from 'react-native';
import {Input, HelperText, IInputProps} from '../../FirstGroup';

interface IInputError extends IInputProps {
  supportMessage?: string;
  errorMessage?: string;
}

const InputError: React.FC<IInputError> = (props) => {
  const {supportMessage, errorMessage} = props;
  const type = errorMessage ? 'error' : 'info';
  const message = errorMessage || supportMessage;
  return (
    <View>
      <Input {...props} error={!!errorMessage} />
      <HelperText message={message} type={type} />
    </View>
  );
};

export default InputError;
