import React from 'react';
import {View, FlatList} from 'react-native';
import {Button, Text} from '../../FirstGroup';

import styles from './ItemListStyle';

type item = {
  title: string;
  onPress: () => void;
};

interface IItemList {
  emptyText?: string;
  data?: item[];
}

const ItemList: React.FC<IItemList> = (props) => {
  const {emptyText = 'Список пуст', data = []} = props;

  const renderItem = ({item}: {item: item}) => {
    return <Button onPress={item.onPress}>{item.title}</Button>;
  };

  const renderEmpty = () => {
    return (
      <View>
        <Text>{emptyText}</Text>
      </View>
    );
  };

  return (
    <FlatList
      contentContainerStyle={!data.length && styles.emptyContainer}
      data={data}
      renderItem={renderItem}
      ListEmptyComponent={renderEmpty}
      keyExtractor={(item) => item.title}
    />
  );
};

export default ItemList;
