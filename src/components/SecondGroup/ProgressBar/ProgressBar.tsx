import React from 'react';
import {Text, View} from 'react-native';

import styles from './ProgressBarStyles';

interface IProgressBarProps {
  count: number;
  step: number;
}

const ProgressBar: React.FC<IProgressBarProps> = (props) => {
  const {count, step} = props;

  const renderElements = () => {
    const array = [1];
    const width = `${100 / count}%`;

    for (let i = 2; i <= count; i++) {
      array.push(i);
    }

    return array.map((index) => {
      const isActive = index <= step;
      const isLast = index === count;
      return (
        <View key={index.toString()} style={[styles.segment, {width}, isActive && !isLast && styles.segmentActive]}>
          <Text style={[styles.number, isActive && styles.numberActive]}>{index}</Text>
        </View>
      );
    });
  };

  return (
    <View style={styles.container}>
      <View style={[styles.filling, {width: `${(step * 100) / count}%`}]} />
      {renderElements()}
    </View>
  );
};

export default ProgressBar;
