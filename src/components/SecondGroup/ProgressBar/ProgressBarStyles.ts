/* eslint-disable react-native/no-color-literals */
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderColor: 'rgb(2, 55, 125)',
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    height: '3%',
    marginVertical: '5%',
    overflow: 'hidden',
    width: '100%',
  },
  filling: {
    backgroundColor: 'rgb(2, 55, 125)',
    height: '100%',
    position: 'absolute',
  },
  number: {
    color: 'rgb(2, 55, 125)',
    fontSize: 12,
    lineHeight: 15,
    textAlign: 'center',
  },
  numberActive: {
    color: 'white',
  },
  segment: {
    alignItems: 'center',
    borderRightColor: 'rgba(2, 55, 125, 0.3)',
    borderRightWidth: 1,
    justifyContent: 'center',
  },
  segmentActive: {
    borderRightColor: 'white',
  },
});
