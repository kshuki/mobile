import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {IRoute, IScreenOption} from '../types';

const Tab = createBottomTabNavigator();

interface IStackProp {
  routes?: IRoute<IScreenOption>[];
}

const BottomTab: React.FC<IStackProp> = (props) => {
  const {routes = []} = props;
  const listScreen = routes.map((route) => (
    <Tab.Screen name={route.name} component={route.component} key={route.name} options={route.options} />
  ));

  return <Tab.Navigator>{listScreen}</Tab.Navigator>;
};

export default BottomTab;
