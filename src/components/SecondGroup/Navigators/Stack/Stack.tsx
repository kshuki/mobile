import React from 'react';
import {createStackNavigator, StackNavigationOptions} from '@react-navigation/stack';
import {IRoute} from '../types';

const Stack = createStackNavigator();

interface IStackProp {
  routes?: IRoute<StackNavigationOptions>[];
}

const StackNavigator: React.FC<IStackProp> = (props) => {
  const {routes = []} = props;
  const listScreen = routes.map((route) => (
    <Stack.Screen name={route.name} component={route.component} key={route.name} options={route.options} />
  ));

  return <Stack.Navigator>{listScreen}</Stack.Navigator>;
};

export default StackNavigator;
