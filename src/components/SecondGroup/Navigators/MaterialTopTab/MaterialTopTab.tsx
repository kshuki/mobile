import React from 'react';
import {createMaterialTopTabNavigator, MaterialTopTabNavigationOptions} from '@react-navigation/material-top-tabs';
import {IRoute} from '../types';

const Tab = createMaterialTopTabNavigator();

interface IStackProp {
  routes?: IRoute<MaterialTopTabNavigationOptions>[];
}

const MaterialTopTab: React.FC<IStackProp> = (props) => {
  const {routes = []} = props;
  const listScreen = routes.map((route) => (
    <Tab.Screen name={route.name} component={route.component} key={route.name} />
  ));

  return <Tab.Navigator>{listScreen}</Tab.Navigator>;
};

export default MaterialTopTab;
