import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
  DrawerNavigationOptions,
  DrawerContentComponentProps,
} from '@react-navigation/drawer';

import {IRoute} from '../types';
// import {ListProject} from './components';

const Drawer = createDrawerNavigator();

interface IDrawerItemProps {
  label: string;
  onPress: (props: DrawerContentComponentProps) => void;
}

interface IDrawerProps {
  routes?: IRoute<DrawerNavigationOptions>[];
  items?: IDrawerItemProps[];
  upElement?: React.ReactNode;
  // initialRouteName?: string;
  // screenOptions?: string;
  // backBehavior?: 'firstRoute' | 'initialRoute' | 'order' | 'history' | 'none';
  // openByDefault?: false;
}

interface IItemRenderProps extends DrawerContentComponentProps {
  upElement?: React.ReactNode;
  items?: IDrawerItemProps[];
}

const ItemRender: React.FC<IItemRenderProps> = (props) => {
  const {items = [], upElement} = props;
  const listItem = items.map((el) => <DrawerItem label={el.label} onPress={() => el.onPress(props)} key={el.label} />);

  // const upElementRender = () => {
  //   if (upElement) {
  //     return upElement;
  //   }
  // };

  return (
    <DrawerContentScrollView {...props}>
      {/* <ListProject /> */}
      {upElement}
      <DrawerItemList {...props} />
      {listItem}
    </DrawerContentScrollView>
  );
};

const DrawerNavigator: React.FC<IDrawerProps> = (props) => {
  const {routes = [], items, upElement} = props;
  const listScreen = routes.map((route) => (
    <Drawer.Screen name={route.name} component={route.component} key={route.name} options={route.options} />
  ));

  return (
    <Drawer.Navigator
      drawerContent={(drawerProps) => <ItemRender items={items} upElement={upElement} {...drawerProps} />}
      screenOptions={{headerShown: true}}
    >
      {listScreen}
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
