import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {IRoute, IScreenOption} from '../types';

const Tab = createMaterialBottomTabNavigator();

interface IStackProp {
  routes?: IRoute<IScreenOption>[];
}

const MaterialBottomTab: React.FC<IStackProp> = (props) => {
  const {routes = []} = props;
  const listScreen = routes.map((route) => (
    <Tab.Screen name={route.name} component={route.component} key={route.name} options={route.options} />
  ));

  return <Tab.Navigator>{listScreen}</Tab.Navigator>;
};

export default MaterialBottomTab;
