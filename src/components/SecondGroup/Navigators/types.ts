import {BottomTabBarButtonProps} from '@react-navigation/bottom-tabs';
import {StyleProp, TextStyle} from 'react-native';

export interface IRoute<T = never> {
  name: string;
  component: React.ComponentType<unknown>;
  options?: T;
}

export interface IScreenOption {
  title?: string;
  tabBarVisible?: boolean;
  tabBarIcon?: (props: {focused: boolean; color: string; size: number}) => React.ReactNode;
  tabBarLabel?: string;
  tabBarBadge?: string | number;
  tabBarBadgeStyle?: StyleProp<TextStyle>;
  tabBarButton?: (props: BottomTabBarButtonProps) => React.ReactNode;
  tabBarAccessibilityLabel?: string;
  tabBarTestID?: string;
}
