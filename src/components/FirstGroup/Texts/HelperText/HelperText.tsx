import React from 'react';
import {HelperText as Helper} from 'react-native-paper';

interface IHelperProps {
  message?: string;
  type?: 'error' | 'info';
}

export const HelperText: React.FC<IHelperProps> = (props) => {
  const {message = '', type = 'info'} = props;
  return (
    <Helper type={type} visible={!!message}>
      {message}
    </Helper>
  );
};

export default HelperText;
