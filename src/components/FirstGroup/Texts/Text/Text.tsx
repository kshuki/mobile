import React from 'react';
import {Text as TextPepper, useTheme} from 'react-native-paper';

interface ITextProps {
  children: string;
  color?: string;
}

const Text: React.FC<ITextProps> = (props) => {
  const {colors} = useTheme();
  const {color = colors.text, children} = props;
  return <TextPepper style={{color}}>{children}</TextPepper>;
};

export default Text;
