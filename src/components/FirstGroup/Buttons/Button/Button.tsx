import React from 'react';
import {Button} from 'react-native-paper';

interface IButtonProps {
  onPress: () => void;
  children: React.ReactNode;
}

const NewButton: React.FC<IButtonProps> = (props) => {
  return <Button mode="contained" {...props} />;
};

export default NewButton;
