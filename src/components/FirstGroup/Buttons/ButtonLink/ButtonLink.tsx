import React from 'react';
import {Button} from 'react-native-paper';

interface ILinkButtonProps {
  title: string;
  onPress: () => void;
}

const LinkButton: React.FC<ILinkButtonProps> = (props) => {
  const {title} = props;
  return <Button {...props}>{title}</Button>;
};

export default LinkButton;
