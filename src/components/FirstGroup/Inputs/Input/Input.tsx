import React from 'react';
import type {KeyboardType} from 'react-native';
import {TextInput} from 'react-native-paper';

export interface IInputProps {
  onChangeText: (text: string) => void;
  label?: string;
  value?: string;
  error?: boolean;
  placeholder?: string;
  keyboardType?: KeyboardType;
  secureTextEntry?: boolean;
}

export const Input: React.FC<IInputProps> = (props) => {
  const {placeholder = 'Введите значение'} = props;
  return <TextInput {...props} placeholder={placeholder} />;
};
