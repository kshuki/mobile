import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  body: {
    flexGrow: 1,
    justifyContent: 'center',
    width: '100%',
  },
  container: {
    alignItems: 'center',
    flex: 1,
    paddingBottom: '5%',
    paddingHorizontal: '4%',
  },
});
