import React from 'react';
import {View} from 'react-native';

import styles from './PageStyles';

interface IPageProps {
  header?: React.ReactNode;
  footer?: React.ReactNode;
  positionBody?: 'center' | 'top' | 'bottom';
}

const Page: React.FC<IPageProps> = (props) => {
  const {header, children, footer} = props;

  const renderBody = () => {
    return <View style={styles.body}>{children}</View>;
  };

  return (
    <View style={styles.container}>
      {header}
      {renderBody()}
      {footer}
    </View>
  );
};

export default Page;
