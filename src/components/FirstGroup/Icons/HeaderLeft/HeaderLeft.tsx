import React from 'react';
import {View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './HeaderLeftStyles';

interface IHeaderLeftProps {
  onPress: () => void;
  icon?: string;
}

const HeaderLeft: React.FC<IHeaderLeftProps> = (props) => {
  const {onPress, icon = 'menu'} = props;
  const {colors} = useTheme();
  return (
    <View style={styles.container}>
      <Icon name={icon} size={24} color={colors.primary} onPress={onPress} />
    </View>
  );
};

export default HeaderLeft;
