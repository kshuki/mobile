import React from 'react';
import {View, Text} from 'react-native';
import {observer} from 'mobx-react-lite';
import {AuthScreenNavigationProp} from '../../types';

import styles from './RegistrationStyles';

interface RegistrationProps {
  navigation: AuthScreenNavigationProp;
}

const Registration: React.FC<RegistrationProps> = observer(() => {
  return (
    <View style={styles.container}>
      <Text>Registration</Text>
    </View>
  );
});

export default Registration;
