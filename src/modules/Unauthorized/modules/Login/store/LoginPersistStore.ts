import {makeAutoObservable} from 'mobx';
import {makePersistable} from 'mobx-persist-store';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class LoginPersistStore {
  login = '';

  constructor() {
    makeAutoObservable(this);
    makePersistable(this, {name: 'LoginPersistStore', properties: ['login'], storage: AsyncStorage});
  }

  setLogin = (login: string): void => {
    this.login = login;
  };
}

export default new LoginPersistStore();
