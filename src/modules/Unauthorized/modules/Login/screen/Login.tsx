import React, {useState} from 'react';
import {View} from 'react-native';
import {observer} from 'mobx-react-lite';
import {ButtonLink, InputError} from 'components';
import {LoginPersistStore, ApplicationStore} from '../store';
import {AuthScreenNavigationProp, UnauthorizedRouts, MainRouts} from '../types';

interface ILoginProps {
  navigation: AuthScreenNavigationProp;
}

const Login: React.FC<ILoginProps> = observer((props) => {
  const {navigation} = props;
  const {setLogin, login} = LoginPersistStore;
  const {setUserAuthorized} = ApplicationStore;

  const [password, setPassword] = useState('');

  const openRegistration = () => {
    navigation.navigate(UnauthorizedRouts.Registration);
  };

  const openWorkspace = () => {
    navigation.navigate(MainRouts.Workspace);
  };

  const onPressLogin = () => {
    setUserAuthorized();
    openWorkspace();
  };

  return (
    <View>
      <InputError
        label="Логин"
        placeholder="Введите логин"
        value={login}
        onChangeText={setLogin}
        keyboardType={'email-address'}
      />
      <InputError
        label="Пароль"
        placeholder="Введите пароль"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <ButtonLink title="Sign in" onPress={onPressLogin} />
      <ButtonLink title="Registration" onPress={openRegistration} />
      <ButtonLink title="Продолжить без авторизации" onPress={openWorkspace} />
    </View>
  );
});

export default Login;
