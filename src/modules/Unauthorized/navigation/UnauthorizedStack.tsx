import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Login, Registration} from '../modules';
import {UnauthorizedRouts, AuthParamList} from '../types';

const Stack = createStackNavigator<AuthParamList>();

const StackNavigator: React.FC = () => {
  return (
    <Stack.Navigator screenOptions={{headerBackTitleVisible: false}}>
      <Stack.Screen name={UnauthorizedRouts.Authorization} component={Login} />
      <Stack.Screen name={UnauthorizedRouts.Registration} component={Registration} />
    </Stack.Navigator>
  );
};

export default StackNavigator;
