import {StackNavigationProp} from '@react-navigation/stack';
import {MainParamList} from '../../../types';

export * from '../../../types';

export enum UnauthorizedRouts {
  Authorization = 'UnauthorizedRoutAuthorization',
  Registration = 'UnauthorizedRoutRegistration',
}

export type AuthParamList = {
  UnauthorizedRoutAuthorization: undefined;
  UnauthorizedRoutRegistration: undefined;
} & MainParamList;

export type AuthScreenNavigationProp = StackNavigationProp<AuthParamList, UnauthorizedRouts.Authorization>;
