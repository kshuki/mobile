import {makeAutoObservable} from 'mobx';
import {makePersistable} from 'mobx-persist-store';
import AsyncStorage from '@react-native-async-storage/async-storage';

export type ProjectType = {
  title: string;
  localId: number;
  id?: number; // принимаем от сервера
  idUser?: number;
};
export class WorkspaceStore {
  countProject = 1;
  selectedProject: null | ProjectType = null;
  listProject: ProjectType[] = [];

  constructor() {
    makeAutoObservable(this);
    makePersistable(this, {
      name: 'WorkspaceStore',
      properties: ['listProject', 'selectedProject', 'countProject'],
      storage: AsyncStorage,
    });
  }

  createProject = (title: string): void | Error => {
    if (!title) {
      return new Error('Empty project name');
    }

    const checkProject = this.listProject.find((project) => project.title === title);
    if (checkProject) {
      return new Error('Duplicate project name');
    }

    const newProject: ProjectType = {title, localId: this.countProject};

    this.listProject.push(newProject);
    this.selectedProject = newProject;
    this.countProject++;
  };

  removeProject = (localId: number): void => {
    const index = this.listProject.findIndex((project) => project.localId === localId);
    if (index !== -1) {
      this.listProject.splice(index, 1);
    }
    if (localId === this.selectedProject.localId) {
      this.selectedProject = null;
    }
  };

  selectProject = (project: ProjectType | null): void => {
    this.selectedProject = project;
  };
}

export default new WorkspaceStore();
