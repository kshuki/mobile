import React from 'react';
import {useTheme} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {observer} from 'mobx-react-lite';
import {Settings, CreateProject, DriverHeader, ProjectBottomTab} from '../modules';
import {WorkspaceRouts, WorkspaceParamList} from '../types';
import {WorkspaceStore} from '../store';

const Drawer = createDrawerNavigator<WorkspaceParamList>();

const WorkspaceDrawer: React.FC = observer(() => {
  const {selectedProject} = WorkspaceStore;
  const {colors} = useTheme();
  const initialRouteName = selectedProject ? WorkspaceRouts.Project : WorkspaceRouts.CreateProject;

  return (
    <Drawer.Navigator
      initialRouteName={initialRouteName}
      drawerContent={(drawerProps) => <DriverHeader {...drawerProps} />}
      screenOptions={{headerShown: true, drawerActiveTintColor: colors.primary}}
    >
      <Drawer.Screen name={WorkspaceRouts.CreateProject} component={CreateProject} options={{headerShown: false}} />
      <Drawer.Screen
        name={WorkspaceRouts.Project}
        component={ProjectBottomTab}
        options={{title: 'Проект', headerShown: false}}
      />
      <Drawer.Screen name={WorkspaceRouts.Settings} component={Settings} options={{title: 'Настройки'}} />
    </Drawer.Navigator>
  );
});

export default WorkspaceDrawer;
