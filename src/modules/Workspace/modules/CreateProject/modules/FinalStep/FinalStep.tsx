import React from 'react';
import {Alert} from 'react-native';
import {Subheading} from 'react-native-paper';
import {RouteProp} from '@react-navigation/native';
import {observer} from 'mobx-react-lite';
import {Page, ProgressBar, Button, Text} from 'components';
import {WorkspaceStore} from './store';
import {NUMBER_OF_STEPS, stepNumber} from '../../constants';
import {CreateProjectParamList, CreateProjectRouts, WorkspaceRouts, CreateProjectNavigationProp} from './types';

interface IMainSettingsProps {
  navigation: CreateProjectNavigationProp;
  nameProject: string;
  route: RouteProp<CreateProjectParamList, CreateProjectRouts.second>;
}

const FinalStep: React.FC<IMainSettingsProps> = observer((props) => {
  const {nameProject, navigation, route} = props;
  const textButton = 'Создать проект';

  const clearHistory = () => {
    const state = navigation.getState();
    const routes = state.routes.filter((r) => r.name === CreateProjectRouts.name);
    navigation.reset({...state, routes, index: routes.length - 1});
  };

  const CreateProject = () => {
    try {
      clearHistory();
      // TODO: Возможно придется вынести createProject в store;
      WorkspaceStore.createProject(nameProject);
      navigation.navigate(WorkspaceRouts.Project);
    } catch (e) {
      Alert.alert('Error', e);
    }
  };

  return (
    <Page
      header={<ProgressBar count={NUMBER_OF_STEPS} step={stepNumber(route.name)} />}
      footer={<Button onPress={CreateProject}>{textButton}</Button>}
    >
      <Subheading>Название приложения:</Subheading>
      <Text>{nameProject}</Text>
    </Page>
  );
});

export default FinalStep;
