import React, {useState, useEffect} from 'react';
import {RouteProp} from '@react-navigation/native';
import {HeaderLeft, ProgressBar, InputError, Page, Button} from 'components';
import {NUMBER_OF_STEPS, stepNumber} from '../../constants';
import {CreateProjectParamList, CreateProjectRouts, CreateProjectNavigationProp} from '../../types';

interface IMainSettingsProps {
  navigation: CreateProjectNavigationProp;
  name: string;
  setName: (name: string) => void;
  showDrawer: (isShow: boolean) => void;
  route: RouteProp<CreateProjectParamList, CreateProjectRouts.name>;
}

const MainSettings: React.FC<IMainSettingsProps> = (props) => {
  const {setName, name, showDrawer, navigation, route} = props;
  const [nameProject, setNameProject] = useState(name);
  const [error, setError] = useState<null | string>(null);

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => <HeaderLeft onPress={() => showDrawer(false)} />,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const CreateProject = () => {
    if (nameProject) {
      setName(nameProject);
      showDrawer(true);
      navigation.navigate(CreateProjectRouts.second);
    } else {
      setError('Имя проекта не может быть пустым');
    }
  };

  return (
    <Page
      header={<ProgressBar count={NUMBER_OF_STEPS} step={stepNumber(route.name)} />}
      footer={<Button onPress={CreateProject}>Далее</Button>}
    >
      <InputError label="Название проекта" value={nameProject} onChangeText={setNameProject} errorMessage={error} />
    </Page>
  );
};

export default MainSettings;
