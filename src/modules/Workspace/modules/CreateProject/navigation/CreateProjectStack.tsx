import React, {useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {MainSettings, FinalStep} from '../modules';
import {CreateProjectParamList, CreateProjectRouts, WorkspaceNavigationProp} from '../types';

const Stack = createStackNavigator<CreateProjectParamList>();

interface IProps {
  navigation: WorkspaceNavigationProp;
}

const StackNavigator: React.FC<IProps> = ({navigation}) => {
  const [name, setName] = useState('');

  const showDrawer = (isClose: boolean) => {
    isClose ? navigation.closeDrawer() : navigation.toggleDrawer();
  };

  return (
    <Stack.Navigator screenOptions={{headerBackTitleVisible: false}}>
      <Stack.Screen name={CreateProjectRouts.name} options={{title: 'Создание проекта'}}>
        {(props) => <MainSettings name={name} setName={setName} showDrawer={showDrawer} {...props} />}
      </Stack.Screen>
      <Stack.Screen name={CreateProjectRouts.second} options={{title: 'Подтвердите данные'}}>
        {(props) => <FinalStep nameProject={name} {...props} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};

export default StackNavigator;
