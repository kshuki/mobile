import {CreateProjectRouts} from '../types';

export const NUMBER_OF_STEPS = Object.keys(CreateProjectRouts).length;

export const stepNumber = (route?: CreateProjectRouts): number => {
  switch (route) {
    case CreateProjectRouts.name:
      return 1;
    case CreateProjectRouts.second:
      return 2;
    default:
      return NUMBER_OF_STEPS;
  }
};
