import {StackNavigationProp} from '@react-navigation/stack';
import {WorkspaceParamList} from '../../../types';

export * from '../../../types';

export enum CreateProjectRouts {
  name = 'CreateProjectRoutName',
  second = 'CreateProjectRoutSecond',
}

export type CreateProjectParamList = {
  CreateProjectRoutName: undefined;
  CreateProjectRoutSecond: undefined;
} & WorkspaceParamList;

export type CreateProjectNavigationProp = StackNavigationProp<CreateProjectParamList, CreateProjectRouts.name>;
