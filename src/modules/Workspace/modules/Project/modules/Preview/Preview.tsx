import React from 'react';
import {View, Text} from 'react-native';
import {observer} from 'mobx-react-lite';
import {WorkspaceNavigationProp} from './types';

import styles from './PreviewStyles';

interface IPreviewProps {
  navigation: WorkspaceNavigationProp;
}

const Preview: React.FC<IPreviewProps> = observer(() => {
  return (
    <View style={styles.container}>
      <Text>Preview</Text>
    </View>
  );
});

export default Preview;
