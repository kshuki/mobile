import {StackNavigationProp} from '@react-navigation/stack';

export * from '../../../types';

export enum CreateProjectRouts {
  name = 'CreateProjectRoutName',
  second = 'CreateProjectRoutSecond',
}

export type CreateProjectParamList = {
  CreateProjectRoutName: undefined;
  CreateProjectRoutSecond: undefined;
};

export type CreateProjectNavigationProp = StackNavigationProp<CreateProjectParamList, CreateProjectRouts.name>;
