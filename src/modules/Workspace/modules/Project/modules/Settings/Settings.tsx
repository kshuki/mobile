import React from 'react';
import {View, Text} from 'react-native';
import {observer} from 'mobx-react-lite';
import {WorkspaceNavigationProp} from './types';

import styles from './SettingsStyles';

interface ISettingsProps {
  navigation: WorkspaceNavigationProp;
}

const Settings: React.FC<ISettingsProps> = observer(() => {
  return (
    <View style={styles.container}>
      <Text>Settings</Text>
    </View>
  );
});

export default Settings;
