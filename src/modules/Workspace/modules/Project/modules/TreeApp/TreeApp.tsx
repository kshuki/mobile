import React from 'react';
import {View, Text} from 'react-native';
import {observer} from 'mobx-react-lite';
import {WorkspaceNavigationProp} from './types';

import styles from './TreeAppStyles';

interface ITreeAppProps {
  navigation: WorkspaceNavigationProp;
}

const TreeApp: React.FC<ITreeAppProps> = observer(() => {
  return (
    <View style={styles.container}>
      <Text>Tree application</Text>
    </View>
  );
});

export default TreeApp;
