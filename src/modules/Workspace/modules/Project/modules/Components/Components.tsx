import React from 'react';
import {observer} from 'mobx-react-lite';
import {Button, ItemList, Page} from 'components';
import {WorkspaceNavigationProp} from './types';

interface IComponentsProps {
  navigation: WorkspaceNavigationProp;
}

const Components: React.FC<IComponentsProps> = observer(() => {
  const createComponent = () => {
    // TODO: Создать компонент
  };

  return (
    <Page footer={<Button onPress={createComponent}>Создать компонент</Button>}>
      <ItemList />
    </Page>
  );
});

export default Components;
