import React from 'react';
import {View, Text} from 'react-native';
import {observer} from 'mobx-react-lite';
import {WorkspaceNavigationProp} from './types';

import styles from './PagesStyles';

interface IPagesProps {
  navigation: WorkspaceNavigationProp;
}

const Pages: React.FC<IPagesProps> = observer(() => {
  return (
    <View style={styles.container}>
      <Text>Pages</Text>
    </View>
  );
});

export default Pages;
