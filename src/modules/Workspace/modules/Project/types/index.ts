import {BottomTabNavigationProp} from '@react-navigation/bottom-tabs';
import {WorkspaceParamList} from '../../../types';

export * from '../../../types';

export enum ProjectRouts {
  Components = 'ProjectRoutComponents',
  Pages = 'ProjectRoutPages',
  TreeApp = 'ProjectRoutTree',
  Settings = 'ProjectRoutSettings',
  Preview = 'ProjectRoutPreview',
}

export type ProjectParamList = {
  ProjectRoutComponents: undefined;
  ProjectRoutPages: undefined;
  ProjectRoutTree: undefined;
  ProjectRoutSettings: undefined;
  ProjectRoutPreview: undefined;
} & WorkspaceParamList;

export type ProjectNavigationProp = BottomTabNavigationProp<ProjectParamList, ProjectRouts.Settings>;
