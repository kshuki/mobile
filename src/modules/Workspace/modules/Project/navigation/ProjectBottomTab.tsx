import React, {useLayoutEffect} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {observer} from 'mobx-react-lite';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {HeaderLeft} from 'components';
import {TreeApp, Settings, Components, Pages, Preview} from '../modules';
import {ProjectParamList, ProjectRouts, WorkspaceNavigationProp, WorkspaceRouts} from '../types';
import {WorkspaceStore} from '../store';

interface IProps {
  navigation: WorkspaceNavigationProp;
}

const BottomTab = createBottomTabNavigator<ProjectParamList>();

const ProjectBottomTab: React.FC<IProps> = observer(({navigation}) => {
  const {selectedProject} = WorkspaceStore;

  useLayoutEffect(() => {
    if (!selectedProject) {
      navigation.navigate(WorkspaceRouts.CreateProject);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedProject]);

  return (
    <BottomTab.Navigator screenOptions={{headerLeft: () => <HeaderLeft onPress={navigation.toggleDrawer} />}}>
      <BottomTab.Screen
        name={ProjectRouts.Components}
        component={Components}
        options={{
          title: 'Компоненты',
          tabBarIcon: ({color, size}) => <MaterialCommunityIcons name={'puzzle-outline'} color={color} size={size} />,
        }}
      />
      <BottomTab.Screen
        name={ProjectRouts.Pages}
        component={Pages}
        options={{
          title: 'Экраны',
          tabBarIcon: ({color, size}) => <Entypo name={'documents'} color={color} size={size} />,
        }}
      />
      <BottomTab.Screen
        name={ProjectRouts.TreeApp}
        component={TreeApp}
        options={{
          title: 'Дерево',
          tabBarIcon: ({color, size}) => <Entypo name={'flow-tree'} color={color} size={size} />,
        }}
      />
      <BottomTab.Screen
        name={ProjectRouts.Settings}
        component={Settings}
        options={{
          title: 'Настройки',
          tabBarIcon: ({color, size}) => <MaterialIcons name={'settings'} color={color} size={size} />,
        }}
      />
      <BottomTab.Screen
        name={ProjectRouts.Preview}
        component={Preview}
        options={{
          title: 'Предпросмотр',
          tabBarIcon: ({color, size}) => <MaterialIcons name={'preview'} color={color} size={size} />,
        }}
      />
    </BottomTab.Navigator>
  );
});

export default ProjectBottomTab;
