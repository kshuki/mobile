import React from 'react';
import {DrawerContentScrollView, DrawerContentComponentProps} from '@react-navigation/drawer';
import {Dropdown, DownList, MiddleList} from './components';

const DriverHeader: React.FC<DrawerContentComponentProps> = (props) => {
  return (
    <DrawerContentScrollView {...props}>
      <Dropdown />
      <MiddleList {...props} />
      <DownList />
    </DrawerContentScrollView>
  );
};

export default DriverHeader;
