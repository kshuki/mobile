import React from 'react';
import {observer} from 'mobx-react-lite';
import {DrawerItem, DrawerContentComponentProps} from '@react-navigation/drawer';
import {CommonActions, DrawerActions} from '@react-navigation/native';
import {WorkspaceStore} from '../../store';
import {WorkspaceRouts} from '../../types';

const MiddleList: React.FC<DrawerContentComponentProps> = observer((props) => {
  if (!WorkspaceStore.selectedProject) {
    return null;
  }
  const {state, descriptors, navigation} = props;
  const drawerItems = state.routes.map((route, i) => {
    if (route.name === WorkspaceRouts.CreateProject) {
      return null;
    }

    const focused = i === state.index;
    const {title, drawerLabel} = descriptors[route.key].options;

    const onPress = () => {
      navigation.dispatch({
        ...(focused
          ? DrawerActions.closeDrawer()
          : CommonActions.navigate({
              name: route.name,
              merge: true,
            })),
        target: state.key,
      });
    };

    return (
      <DrawerItem
        key={route.key}
        focused={focused}
        label={drawerLabel !== undefined ? drawerLabel : title !== undefined ? title : route.name}
        onPress={onPress}
      />
    );
  });

  return <React.Fragment>{drawerItems}</React.Fragment>;
});

export default MiddleList;
