import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  buttonContainer: {
    // backgroundColor: 'green',
    alignItems: 'center',
    flexDirection: 'row',
    // flex: 1,
    // marginRight: 32,

    justifyContent: 'space-between',
    marginHorizontal: 8,
    // marginVertical: 4,
  },
  container: {
    marginHorizontal: 10,
    marginVertical: 4,
    overflow: 'hidden',
  },
});
