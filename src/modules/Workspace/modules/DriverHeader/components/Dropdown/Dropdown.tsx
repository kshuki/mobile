import React, {useState} from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import {observer} from 'mobx-react-lite';
import {DrawerItem} from '@react-navigation/drawer';
import {useNavigation, useTheme} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';
import {Text} from 'components';
import {WorkspaceStore} from '../../../../store';
import {WorkspaceRouts, WorkspaceNavigationProp} from '../../../../types';

import styles from './DropdownStyles';

const Dropdown: React.FC = observer(() => {
  const [isOpen, setIsOpen] = useState(false);
  const navigation = useNavigation<WorkspaceNavigationProp>();
  const {colors} = useTheme();
  const {selectedProject, listProject, selectProject} = WorkspaceStore;

  // TODO: Временный костыль, ждем пока не появится юзер.
  // После авторизации текущему проекту должен присваиваться Id;
  // useEffect(() => {
  //   if (!selectedProject?.idUser) {
  //     selectProject(null);
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  const renderProjects = () => (
    <React.Fragment>
      {listProject.map((item) => {
        if (item === selectedProject) {
          return null;
        }
        return (
          <DrawerItem
            label={item.title}
            key={item.localId}
            onPress={() => {
              selectProject(item);
              navigation.navigate(WorkspaceRouts.Project);
            }}
          />
        );
      })}
      <DrawerItem
        label="Создать проект"
        onPress={() => {
          navigation.navigate(WorkspaceRouts.CreateProject);
        }}
      />
    </React.Fragment>
  );

  const onPress = () => {
    setIsOpen((prevState) => {
      return !prevState;
    });
  };

  const renderCurrentComponent = () => {
    const title = selectedProject ? selectedProject.title : 'Выберите проект';
    const icon = isOpen ? 'up' : 'down';
    const color = colors.primary;

    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.buttonContainer}>
            <Text color={color}>{title}</Text>
            <Icon name={icon} size={15} color={color} />
          </View>
        </TouchableWithoutFeedback>
        {isOpen && renderProjects()}
      </View>
    );
  };

  return renderCurrentComponent();
});

export default Dropdown;
