import React from 'react';
import {observer} from 'mobx-react-lite';
import {useNavigation} from '@react-navigation/native';
import {DrawerItem} from '@react-navigation/drawer';
import {ApplicationStore} from '../../store';
import {WorkspaceNavigationProp, MainRouts} from '../../types';

type Item = {
  label: string;
  onPress: () => void;
};

const DownList: React.FC = observer(() => {
  const {isAuthorization, setUserUnauthorized} = ApplicationStore;
  const navigate = useNavigation<WorkspaceNavigationProp>().navigate;
  const items: Item[] = [];

  if (isAuthorization) {
    items.push({
      label: 'Выйти',
      onPress: () => {
        setUserUnauthorized();
        navigate(MainRouts.Unauthorized);
      },
    });
  } else {
    items.push({label: 'Войти', onPress: () => navigate(MainRouts.Unauthorized)});
  }

  return (
    <React.Fragment>
      {items.map((el) => (
        <DrawerItem label={el.label} onPress={el.onPress} key={el.label} />
      ))}
    </React.Fragment>
  );
});

export default DownList;
