import {DrawerNavigationProp} from '@react-navigation/drawer';
import {MainParamList} from '../../../types';

export * from '../../../types';

export enum WorkspaceRouts {
  CreateProject = 'WorkspaceRoutCreateProject',
  Project = 'WorkspaceRoutProject',
  Settings = 'WorkspaceRoutSettings',
}

export type WorkspaceParamList = {
  WorkspaceRoutCreateProject: undefined;
  WorkspaceRoutProject: undefined;
  WorkspaceRoutSettings: undefined;
} & MainParamList;

export type WorkspaceNavigationProp = DrawerNavigationProp<WorkspaceParamList, WorkspaceRouts.Settings>;
