import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import AntIcon from 'react-native-vector-icons/AntDesign';
import {PaperTheme} from 'theme';
import {AppNavigation} from './navigation';

AntIcon.loadFont();

export default (): JSX.Element => (
  <PaperProvider theme={PaperTheme}>
    <AppNavigation />
  </PaperProvider>
);
