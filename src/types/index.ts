import {StackNavigationProp} from '@react-navigation/stack';

export enum MainRouts {
  Unauthorized = 'MainRoutUnauthorized',
  Authorized = 'MainRoutAuthorized',
  Workspace = 'MainRoutsWorkspace',
  // Preview = 'PreviewRout',
}

export type MainParamList = {
  MainRoutUnauthorized: undefined;
  MainRoutsWorkspace: undefined;
};

export type AuthScreenNavigationProp = StackNavigationProp<MainParamList, MainRouts.Unauthorized>;
