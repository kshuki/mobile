import {makeAutoObservable} from 'mobx';

export class ApplicationStore {
  isAuthorization = false;

  constructor() {
    makeAutoObservable(this);
  }

  setUserAuthorized = (): void => {
    this.isAuthorization = true;
  };

  setUserUnauthorized = (): void => {
    this.isAuthorization = false;
  };
}

export default new ApplicationStore();
