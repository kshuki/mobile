import {DefaultTheme, Theme} from '@react-navigation/native';
import colors from './colors';

const NavigationTheme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.white,
    background: colors.lightGray,
    card: colors.blue,
    text: colors.white,
    notification: colors.white,
  },
};

export default NavigationTheme;
