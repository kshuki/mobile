import {DefaultTheme} from 'react-native-paper';

import colors from './colors';

const PaperTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.blueLight,
    // background: colors.lightGray,
    // placeholder: 'red',
    // text: colors.blue,
    // notification: colors.white,
    // border: 'white',
  },
};

export default PaperTheme;
