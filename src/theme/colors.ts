export default {
  black: 'rgb(0,0,0)',
  blueLight: 'rgb(29, 103, 179)',
  blue: 'rgb(7, 53, 118)',
  lightGray: 'rgb(239, 239, 244)',
  white: 'rgb(255,255,255)',
};
