import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationTheme} from 'theme';
import {WorkspaceDrawer, UnauthorizedStack} from '../modules';
import {MainRouts, MainParamList} from '../types';

const Stack = createStackNavigator<MainParamList>();

const AppNavigation: React.FC = () => {
  return (
    <NavigationContainer theme={NavigationTheme}>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name={MainRouts.Unauthorized} component={UnauthorizedStack} options={{animationEnabled: false}} />
        <Stack.Screen name={MainRouts.Workspace} component={WorkspaceDrawer} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
