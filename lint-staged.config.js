module.exports = {
  '*.{js,jsx,ts,tsx}': ['eslint --fix', 'yarn lint', 'yarn test --bail --findRelatedTests'],
  '**/*.ts?(x)': () => 'tsc -p tsconfig.json --noEmit',
};
