import {ApplicationStore} from '../../src/store/ApplicationStore';

describe('AuthorizationStore', () => {
  it('creates new application', () => {
    const store = new ApplicationStore();
    expect(store.isAuthorization).toBe(false);
  });

  it('authorized checked', () => {
    const store = new ApplicationStore();
    store.setUserAuthorized();
    expect(store.isAuthorization).toBe(true);
  });

  it('unauthorized checked', () => {
    const store = new ApplicationStore();
    store.setUserUnauthorized();
    expect(store.isAuthorization).toBe(false);
  });
});
