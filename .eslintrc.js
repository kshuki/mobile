module.exports = {
  env: {
    es2021: true,
    'react-native/react-native': true,
  },

  parserOptions: {
    sourceType: 'module',
  },

  extends: [
    'plugin:prettier/recommended', // https://github.com/prettier/eslint-plugin-prettier#recommended-configuration
    'prettier/react',
    '@react-native-community',
    'plugin:react-native/all',
    'plugin:@typescript-eslint/recommended',
  ],

  plugins: ['import', 'eslint-comments', 'react', 'react-hooks', 'react-native', '@react-native-community', 'jest'],

  overrides: [
    {
      files: ['*.js'],
      parser: 'babel-eslint',
      plugins: ['flowtype'],
      rules: {
        // Flow Plugin
        // The following rules are made available via `eslint-plugin-flowtype`
        'flowtype/define-flow-type': 1,
        'flowtype/use-flow-type': 1,
      },
    },
    {
      files: ['*.ts', '*.tsx'],
      parser: '@typescript-eslint/parser',
      plugins: ['@typescript-eslint/eslint-plugin'],
      rules: {
        '@typescript-eslint/no-unused-vars': ['error', {argsIgnorePattern: '^_'}],
        'no-unused-vars': 'off',
      },
    },
    {
      files: ['*.{spec,test}.{js,ts,tsx}', '**/__{mocks,tests}__/**/*.{js,ts,tsx}'],
      env: {
        jest: true,
        'jest/globals': true,
      },
      rules: {
        'react-native/no-inline-styles': 0,
      },
    },
    {
      // enable the rule specifically for TypeScript files
      files: ['*.ts', '*.tsx'],
      parser: '@typescript-eslint/parser',
      plugins: ['@typescript-eslint'],
      rules: {
        '@typescript-eslint/explicit-module-boundary-types': 'warn',
        '@typescript-eslint/no-var-requires': 'warn',
        '@typescript-eslint/no-empty-function': 'warn',
      },
    },
  ],

  root: true,

  settings: {
    'import/ignore': ['node_modules/react-native/index\\.js$', 'react-native-keychain', 'react-native'],
    'import/resolver': {
      'babel-module': {
        root: ['./src'],
        extensions: ['.js', '.ts', '.tsx', '.ios.js', '.android.js'],
      },
      node: {
        paths: ['src'],
        extensions: ['.js', '.ts', '.tsx'],
      },
    },
  },

  rules: {
    'no-console': ['error', {allow: ['warn', 'disableYellowBox']}],
    'no-unused-vars': ['error', {vars: 'all', args: 'after-used', ignoreRestSiblings: true}],
    'comma-dangle': 'off',
    'no-bitwise': ['error', {allow: ['~']}],
    'no-shadow': 'off',
    'react-native/no-raw-text': [
      2,
      {
        skip: ['H1', 'H2', 'H3', 'Button', 'Subheading'],
      },
    ],
    quotes: 'off',
    'prettier/prettier': ['error', {singleQuote: true}],
    'import/no-unresolved': [2, {caseSensitive: false}],
    'import/named': 'error',
    'import/default': 'off',
    'import/namespace': 'off',
    'import/export': 'error',
    'import/no-named-as-default': 'error',
    'import/no-named-as-default-member': 'error',
    'import/no-deprecated': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/no-mutable-exports': 'error',
    'import/no-commonjs': 'off',
    'import/no-amd': 'error',
    'import/no-nodejs-modules': 'off',
    'import/first': 'error',
    'import/imports-first': 'off',
    'import/no-duplicates': 'error',
    'import/no-namespace': 'off',
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
    'import/order': ['error', {groups: [['builtin', 'external', 'internal']]}],
    'import/newline-after-import': 'error',
    'import/prefer-default-export': 'off',
    'import/no-restricted-paths': 'off',
    'import/max-dependencies': ['off', {max: 10}],
    'import/no-absolute-path': 'error',
    'import/no-dynamic-require': 'error',
    'import/no-internal-modules': [
      'off',
      {
        allow: [],
      },
    ],
    'import/unambiguous': 'off',
    'import/no-webpack-loader-syntax': 'error',
    'import/no-unassigned-import': 'off',
    'import/no-named-default': 'error',
    'import/no-anonymous-default-export': [
      'off',
      {
        allowArray: false,
        allowArrowFunction: false,
        allowAnonymousClass: false,
        allowAnonymousFunction: false,
        allowLiteral: false,
        allowObject: false,
      },
    ],
    'import/exports-last': 'off',
    'import/group-exports': 'off',
    'import/no-default-export': 'off',
    'import/no-named-export': 'off',
    'import/no-self-import': 'error',
    'import/no-cycle': ['error'],
    'import/no-useless-path-segments': ['error', {commonjs: true}],
    'import/dynamic-import-chunkname': [
      'off',
      {
        importFunctions: [],
        webpackChunknameFormat: '[0-9a-zA-Z-_/.]+',
      },
    ],
    'import/no-relative-parent-imports': 'off',
    'import/no-unused-modules': [
      'off',
      {
        ignoreExports: [],
        missingExports: true,
        unusedExports: true,
      },
    ],
    '@typescript-eslint/no-shadow': ['error'],
    '@typescript-eslint/no-unused-vars': ['error', {ignoreRestSiblings: true}],
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/no-empty-function': 'off',
  },
};
